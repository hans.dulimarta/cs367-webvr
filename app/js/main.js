// Bitly link https://bit.ly/2GO3zYu
import * as THREE from 'three';
import VRControls from 'three-vrcontrols-module';
import WebVRPolyfill from 'webvr-polyfill';
import WebVRLogo from '../images/webvr-logo-square.png';
import CISLogo from '../images/CISLogo.png'
// import orbit from 'three-orbit-controls';
// const OrbitControls = orbit(THREE);
import TrackballControls from 'three-trackballcontrols';

export default class App {
  constructor() {
    const c = document.getElementById('mycanvas');
    // Enable antialias for smoother lines
    this.renderer = new THREE.WebGLRenderer({canvas: c, antialias: true});
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(75, 4/3, 0.1, 500);
    // this.camera.position.z = 100;

    // const orbiter = new OrbitControls(this.camera);
    // orbiter.enableZoom = false;
    // orbiter.update();
    
    const boxGeom = new THREE.BoxGeometry(15,15,15);
    const loader = new THREE.TextureLoader();
    const vrImg = loader.load(WebVRLogo);
    const cisImg = loader.load(CISLogo);
    const vrTex = new THREE.MeshPhongMaterial({map: vrImg});
    const cisTex = new THREE.MeshPhongMaterial({map: cisImg});
    for (let k = 0; k < 100; k++) {
      const randColor = Math.random() * 0xFFFFFF;
      const boxMatr = new THREE.MeshPhongMaterial({ color: randColor })
      const boxMesh = new THREE.Mesh(boxGeom, [vrTex, boxMatr, 
        cisTex, vrTex, 
        cisTex, boxMatr]);
      boxMesh.position.set(Math.random() * 200 -100, Math.random() * 200 - 100, Math.random() * 200 - 100);
      this.scene.add(boxMesh);
    }

    const light = new THREE.PointLight(0xa5d882, 2.0);
    light.position.set(20, 30, 50);
    // const lightHelper = new THREE.DirectionalLightHelper(light, 5);
    this.scene.add(light);

    this.scene.add(new THREE.AxesHelper(20));
    window.addEventListener('resize', () => this.resizeHandler());
    this.resizeHandler();
    const polyfill = new WebVRPolyfill();
    const txtOut = document.getElementById("textout");
    navigator.getVRDisplays().then(displays => {
      if (displays.length) {
        const vlogo = document.getElementById("logo");
        vlogo.hidden = false;
        txtOut.innerText = `WebVR version ${WebVRPolyfill.version}`;
        this.display = displays[0];
        this.camCtrl = new VRControls(this.camera);
      } else {
        vlogo.hidden = false;
        txtOut.innerText = 'No VR Support. Mouse: Left=rotate, Middle=zoom, Right=pan';

        this.camCtrl = new TrackballControls(this.camera);
        this.camCtrl.rotateSpeed = 2.0;
        this.camCtrl.noZoom = false;
        this.camCtrl.noPan = false;
        this.display = window;
      }
      this.display.requestAnimationFrame(() => this.render());
    });
  }

  render() {
    this.renderer.render(this.scene, this.camera);
    this.camCtrl.update();
    this.display.requestAnimationFrame(() => this.render());
  }

  resizeHandler() {
    const canvas = document.getElementById("mycanvas");
    let w = window.innerWidth - 16;
    let h = 0.75 * w;  /* maintain 4:3 ratio */
    if (canvas.offsetTop + h > window.innerHeight) {
      h = window.innerHeight - canvas.offsetTop - 16;
      w = 4/3 * h;
    }
    canvas.width = w;
    canvas.height = h;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(w, h);
    if (this.camCtrl != null && this.camCtrl.handleResize != null)
      this.camCtrl.handleResize();
  }
}