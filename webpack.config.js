var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname);
const ENTRY_PATH = path.resolve(ROOT_PATH, "app/js/index.js");
const JS_PATH = path.resolve(ROOT_PATH, "app/js");
const TEMPLATE_PATH = path.resolve(ROOT_PATH, "app/index.html");

var debug = process.env.NODE_ENV !== 'production';

module.exports = {
  entry: ENTRY_PATH,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "WebVR Demo",
      template: TEMPLATE_PATH,
      inject: 'body'
    }),
    new CopyWebpackPlugin([{from: 'app/images', to: 'images'}])
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: JS_PATH,
        exclude: /node_module/,
        use: 'babel-loader'
      },
      {
        test: /\.(png|jpg|jpeg)$/,
        include: path.resolve(__dirname, 'app/images'),
        use: 'file-loader'
      }
    ]
  }
};
